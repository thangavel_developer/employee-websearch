## INSTALLATION

If application not run, Please run the composer install/update command to load library of laravel and its dependency defined in the composer.json

-> composer update

## ENV

Global values are stored in Environment(.env) file. Need to Change a DB_HOST,DB_USERNAME,DB_PASSWORD, APP_DEBUG(If run locally)

## DATABASE

Create a database name 'employee_audit_logs' in MySQL. Run the following artisan commands to create a tables and values.

1) Create a employees, employee_we_history tables will be created.
  -> php artisan migrate
2) Seeding the employees, employee_we_history tables
  -> php artisan db:seed

## CUSTOM COMMANDS

The commands are run with suffix of php artisan commands. We have a few custom commands to perform a same API Operations 

- php artisan SET 
	<type>          Operation Type (required)
	<arg1>          Employee ID/IP Address
	<arg2>          Employee Name/URL 
	<arg3>          Employee IP Address 

Possible Commands as
 1) Insert the employee details to employee table with data emp_id, emp_name, ip_address.
    -> SET empdata [emp_id] [emp_name] [ip_address]      

 2) It will first check if the ip address is assigned to any employee or not if the ip address is there then it will insert the url variable [url] to the mapped ip_address [ip_address], other with it will throw error.
	-> SET empwebhistory [ip_address] [url]​              


- php artisan GET 
   <type>          Operation Type (required)
   <ip_address>    Employee IP Address (required)

Possible Commands as
 1) Get the employee details having the ip_address
    -> GET empdata [ip_address]    

 2) Get the employee details with his web search history stored under the variable ip_address
	-> GET empwebhistory [ip_address]  

- php artisan UNSET 
	<type>          Operation Type (required)
	<ip_address>    Employee IP Address (required)

Possible Commands as
 1) Soft delete the data having the passed ip_address
    -> UNSET empdata [ip_address]    

 2) :Delete all the web search history data mapped with ip_address.
	-> UNSET empwebhistory [ip_address]


## API ENDPOINTS

We have list API of Employees web search history which supports the HTTP METHOD (GET, POST, DELETE) as follows.

METHOD:GET 

API:/empwebhistory/{ip_address} 
API:/empdata/{ip_address}

METHOD:POST 

API:/empwebhistory
Sample Request: {"ip_address":"192.168.0.123", "url":"http://google.com" }

API:/empdata
Sample Request: {"ip_address":"192.168.0.123","emp_id":"1","emp_name":"Jack"}


METHOD: DELETE: 

API:/empwebhistory/{ip_address}
API:/empdata/{ip_address}


## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
