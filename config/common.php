<?php

return array(
    'create_success_response' => array(
        'content' => array(),
        'response' => array(
            'error' => 0,
            'sys_msg' => '',
            'message' => 'Successfully created!'
        )
    ),
    'create_failure_response' => array(
        'content' => array(),
        'response' => array(
            'error' => 1,
            'sys_msg' => '',
            'message' => 'Failed to create!'
        )
    ),
    'retrieve_failure_response' => array(
        'content' => array(),
        'response' => array(
            'error' => 1,
            'sys_msg' => '',
            'message' => 'Resource not found'
        )
    ),
    'retrieve_success_response' => array(
        'content' => array(),
        'response' => array(
            'error' => 0,
            'sys_msg' => '',
            'message' => 'Success'
        )
    ),
    'delete_failure_response' => array(
        'content' => array(),
        'response' => array(
            'error' => 1,
            'sys_msg' => '',
            'message' => 'Resource delete failed'
        )
    ),
    'delete_success_response' => array(
        'content' => array(),
        'response' => array(
            'error' => 0,
            'sys_msg' => '',
            'message' => 'Successfully Deleted'
        )
    ),
    'failure_response' => array(
        'content' => array(),
        'response' => array(
            'error' => 1,
            'sys_msg' => '',
            'message' => 'failed'
        )
    ),
    'success_response' => array(
        'content' => array(),
        'response' => array(
            'error' => 0,
            'sys_msg' => '',
            'message' => 'Success!'
        )
    )
);
