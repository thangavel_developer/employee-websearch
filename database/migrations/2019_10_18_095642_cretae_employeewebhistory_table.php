<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CretaeEmployeewebhistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('employee_web_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip_address',100)->index()->nullable();
            $table->string('url',255)->nullable();
            $table->date('date')->nullable();
            $table->timestamps();
            //$table->softDeletes();
            $table->foreign('ip_address')->references('ip_address')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_web_history');
    }
}
