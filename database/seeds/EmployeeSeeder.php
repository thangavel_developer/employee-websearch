<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('employees')->delete();

        \App\Employees::create(['emp_id' => '1','emp_name' => 'Jack','ip_address' => '192.168.0.10']);
    }
}
