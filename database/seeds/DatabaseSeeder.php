<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(EmployeeSeeder::class);
         $this->command->info('Employees table seeded!');
         $this->call(EmployeesWebHistorySeeder::class);
         $this->command->info('Employee_Web_History table seeded!');
    }
}
