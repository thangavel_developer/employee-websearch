<?php

use Illuminate\Database\Seeder;

class EmployeesWebHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        \DB::table('employee_web_history')->delete();

        \App\Employee_Web_History::create(['ip_address' => '192.168.0.10','url' => 'http://www.google.com/','date'=>date('Y-m-d')]);
        \App\Employee_Web_History::create(['ip_address' => '192.168.0.10','url' => 'http://www.facebook.com/','date'=>date('Y-m-d')]);
    }
}
