<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\EmployeesController;

use App\Http\Controllers\EmployeeWebHistoryController;

class GETCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'GET {type : Operation Type (required)} {ip_address : Employee IP Address (required)}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Retrieve the resource from Database';

    protected $help = 'GET 
                       <type>          Operation Type (required)
                       <ip_address>    Employee IP Address (required)';

    public function __construct()
    {
        parent::__construct();

        $this->setHelp($this->help);
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // argument
        $type = $this->argument('type');
        $ip_address = $this->argument('ip_address');
        if(empty($type)){
            $this->error('Operation Type not exists');
        }
        else if(empty($ip_address)){
            $this->error('IP Address not exists');
        }
        if(!empty($type) && !empty($ip_address)){
            switch($type){
                case 'empdata':
                    $controller = new EmployeesController(); // make sure to import the controller
                    $response = $controller->show($ip_address);
                    if($response['response']['error'] == 1){
                        $this->info('"empdata":Resource not found');
                    }else
                    {
                       $this->info('"empdata":'.json_encode($response['content'], JSON_PRETTY_PRINT));
                    }
                    break;
                case 'empwebhistory':
                    $controller = new EmployeeWebHistoryController(); // make sure to import the controller
                    $response = $controller->show($ip_address);
                    if($response['response']['error'] == 1){
                        $this->info('"empwebhistory":Resource not found');
                    }else
                    {
                       $this->info('"empwebhistory":'.json_encode($response['content'], JSON_PRETTY_PRINT));
                    }
                    break;
                default:
                    $this->error('Requested Operation not found');
            }
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
