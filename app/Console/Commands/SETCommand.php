<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\EmployeesController;

use App\Http\Controllers\EmployeeWebHistoryController;

use Illuminate\Http\Request;

class SETCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'SET {type : Operation Type (required)} {arg1 : Employee ID/IP Address (required)} {arg2 : Employee Name/URL (required)} {arg3? : Employee IP Address}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Store the resource in Database';


    protected $help = 'SET 
                     <type>          Operation Type (required)
                     <arg1>          Employee ID/IP Address
                     <arg2>          Employee Name/URL 
                     <arg3>          Employee IP Address 
            ';

    public function __construct()
    {
        parent::__construct();

        $this->setHelp($this->help);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // argument
        $type = $this->argument('type');
        if(!empty($type)){            
            $arg1 = $this->argument('arg1');
            $arg2 = $this->argument('arg2');         
            switch ($type) {
                case 'empwebhistory':
                    if(empty($arg1)){
                        $this->info('IP Address not exists');
                    }
                    elseif(empty($arg2)){
                        $this->info('URL not exists');
                    }
                    if(!empty($arg1) && !empty($arg2)){
                        $create_array = array(
                            'ip_address' => $arg1,
                            'url' => $arg2,
                            'date' => date('Y-m-d')
                        );
                        $controller = new EmployeeWebHistoryController(); // make sure to import the controller
                        $response = $controller->store_employee_web_history($create_array);
                        if($response['response']['error'] == 1){
                            $this->info('"empdata":Resource not found');
                        }else
                        {
                            $status = $response['response']['error'] == 0 ? 'Store the resource in Database success' : 'Store the resource in Database failed';
                            $this->info($status);
                        }
                    }
                    break;
                case 'empdata':
                    $arg3 = $this->argument('arg3');
                    if(empty($arg1)){
                        $this->info('Employee ID not exists');
                    }
                    elseif(empty($arg2)){
                        $this->info('Employee Name not exists');
                    }
                    elseif(empty($arg3)){
                        $this->info('IP Address not exists');
                    }
                    if(!empty($arg1) && !empty($arg2) && !empty($arg3)){
                        $create_array = array(
                            'emp_id' => $arg1,
                            'emp_name' => $arg2,
                            'ip_address' => $arg3
                        );
                        $controller = new EmployeesController(); // make sure to import the controller
                        $response = $controller->store_employees_data($create_array);
                        if($response['response']['error'] == 1){
                            $this->info('"empdata":Resource not found');
                        }else
                        {
                            $status = $response['response']['error'] == 0 ? 'Store the resource in Database success' : 'Store the resource in Database failed';
                            $this->info($status);
                        }
                    }
                    break;
                default:
                    $this->error('Requested Operation not found');
            }
        }
        else{
            $this->error('Operation Type not exists');
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
