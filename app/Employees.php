<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employees extends Model
{
    use SoftDeletes;

    /**
     * The mapping between model with table.
     *
     * @var string
     */

    protected $table = 'employees';     
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     * 'payment_transaction_id', 'payment_status' ignored
     * @var array
     */
    protected $fillable = [
    	'emp_id', 'emp_name', 'ip_address'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'created_at','updated_at', 'deleted_at'    
    ];

    /**
     * Get the history for the employee.
     */
    public function history()
    {
        return $this->hasMany('\App\Employee_Web_History','ip_address','ip_address');
    }
}
