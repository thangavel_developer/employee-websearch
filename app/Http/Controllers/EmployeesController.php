<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Log;

class EmployeesController extends Controller
{

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($ip_address)
    {
    	/*$command = 'GET empdata '.$ip_address;
    	$artisan = \Artisan::call($command);
        $output = \Artisan::output();
        return $output;*/

        $success = \Config::get('common.retrieve_success_response');
        $failure = \Config::get('common.retrieve_failure_response');
        try{
        	$check_ip_address = \App\Employees::where('ip_address',$ip_address)->get();
            if($check_ip_address->isEmpty()){
            	Log::info('empdata: show:: Resource not found for ip_address('.$ip_address.')');
            	$failure['response']['message'] = 'Resource not found';
                return $failure;
            }
            $ip_address_data = $check_ip_address->toArray();
            $success['content'] = $ip_address_data;
            Log::info('empdata: show:: Retrived data for ip_address('.$ip_address.')');
            return $success;
        }
        catch(\Exception $e){
        	Log::error('empdata: index:: Request Failed on exception'.$e->getMessage());
        	return $failure;
        }
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = \Config::get('common.create_success_response');
        $failure = \Config::get('common.create_failure_response');
        $input_params = $request->all();
        $validator    = \Validator::make($input_params, array(
            'emp_id' => 'required',
            'emp_name' => 'required',
            'ip_address' => 'required'
        ));
        if ($validator->fails())
          {
            $error = json_decode($validator->errors(), true);
            $message = 'We are unable to process your request as following few field(s) are missing - ' . implode(', ', array_keys($error));
            Log::error('empdata: store:: Request Failed on validation'.$message);
            $failure['response']['message'] = 'We are unable to process your request as following few field(s) are missing - ' . implode(', ', array_keys($error));
            return $failure;
          }
        try{
            return $this->store_employees_data($input_params);
        }
        catch(\Exception $e){
        	Log::error('empdata: store:: Request Failed on exception'.$e->getMessage());
        	return $failure;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ip_address)
    {
        $success = \Config::get('common.delete_success_response');
        $failure = \Config::get('common.delete_failure_response');
        try{
        	$check_ip_address = \App\Employees::where('ip_address',$ip_address)->get();
            if($check_ip_address->isEmpty()){
            	Log::info('empdata: destroy:: Resource not found for ip_address('.$ip_address.')');
            	$failure['response']['message'] = 'Resource not found';
                return $failure;
            }
            $delete_status = \App\Employees::where('ip_address',$ip_address)->delete();
            Log::info('empdata: destroy:: Soft delete the data having the passed ip_address('.$ip_address.')');
            return $delete_status ? $success : $failure;
        }
        catch(\Exception $e){
        	Log::error('empdata: destroy:: Request Failed on exception'.$e->getMessage());
        	return $failure;
        }
    }

    public function store_employees_data($input_params)
    {
    	$success = \Config::get('common.create_success_response');
        $failure = \Config::get('common.create_failure_response');
    	$create_array = array(
            'emp_id' => $input_params['emp_id'],
            'emp_name' => $input_params['emp_name'],
            'ip_address' => $input_params['ip_address']
        );
        extract($create_array);
        $check_ip_address = \App\Employees::where('emp_id',$emp_id)->where('ip_address',$ip_address)->get();
        if($check_ip_address->isEmpty()){
        	Log::info('empdata: store:: Checked and resource not found for ip_address('.$ip_address.'). Create a new record');
            $crud_status = \App\Employees::create($create_array);
        }else{
        	Log::info('empdata: store:: Checked and update a resource which found for ip_address('.$ip_address.').');
        	$crud_status = \App\Employees::where('emp_id',$emp_id)->where('ip_address',$ip_address)->update($create_array);
            //$status = $crud_status ? 'Store the resource in Database success' : 'Store the resource in Database failed';
        }
        return $crud_status ? $success : $failure;
    }
}
