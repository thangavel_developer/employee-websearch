<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;

class EmployeeWebHistoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($ip_address)
    {
        $success = \Config::get('common.retrieve_success_response');
        $failure = \Config::get('common.retrieve_failure_response');
        try{
            $ip_address_history = array();
            $history =\App\Employee_Web_History::where('ip_address',$ip_address)->select('id','ip_address','url')->get();
            if($history->isEmpty()){
                Log::info('empwebhistory: show:: Resource not found for ip_address('.$ip_address.')');
                $failure['response']['message'] = 'Resource not found';
                return $failure;
            }
            $history = $history->toArray();
            $ip_address_history = reset($history);
            $ip_address_history['urls'] = array_unique(array_column($history, 'url'));
            unset($ip_address_history['url']);
            $success['content'] = $ip_address_history;
            Log::info('empwebhistory: show:: Retrived data for ip_address('.$ip_address.').');
            return $success;            
        }
        catch(\Exception $e){
            Log::error('empwebhistory: index: Request Failed on exception'.$e->getMessage());
            return $failure;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = \Config::get('common.create_success_response');
        $failure = \Config::get('common.create_failure_response');
        $input_params = $request->all();
        $validator    = \Validator::make($input_params, array(
            'ip_address' => 'required',
            'url' => 'required'
        ));
        if ($validator->fails())
          {
            $error = json_decode($validator->errors(), true);
            $message = 'We are unable to process your request as following few field(s) are missing - ' . implode(', ', array_keys($error));
            Log::error('empdata: store:: Request Failed on validation'.$message);
            $failure['response']['message'] = 'We are unable to process your request as following few field(s) are missing - ' . implode(', ', array_keys($error));
            return $failure;
          }
        try{
            return $this->store_employee_web_history($input_params);            
        }
        catch(\Exception $e){
            Log::error('empdata: store:: Request Failed on exception'.$e->getMessage());
            return $failure;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ip_address)
    {
        $success = \Config::get('common.delete_success_response');
        $failure = \Config::get('common.delete_failure_response');
        try{
            $check_ip_address = \App\Employee_Web_History::where('ip_address',$ip_address)->get();
            if($check_ip_address->isEmpty()){
                Log::info('empwebhistory: destroy:: Resource not found for ip_address('.$ip_address.')');
                $failure['response']['message'] = 'Resource not found';
                return $failure;
            }
            $delete_status = \App\Employee_Web_History::where('ip_address',$ip_address)->delete();
            Log::info('empwebhistory: destroy:: Deeleted the data having the passed ip_address('.$ip_address.')');
            return $delete_status ? $success : $failure;
        }
        catch(\Exception $e){
            Log::error('empwebhistory: destroy:: Request Failed on exception'.$e->getMessage());
            return $failure;
        }   
    }

    public function store_employee_web_history($input_params)
    {
        $success = \Config::get('common.create_success_response');
        $failure = \Config::get('common.create_failure_response');
        $create_array = array(
            'ip_address' => $input_params['ip_address'],
            'url' => $input_params['url'],
            'date' => date('Y-m-d')
        );
        extract($create_array);
        $check_ip_address = \App\Employees::where('ip_address',$ip_address)->get();
        if($check_ip_address->isEmpty()){
            Log::info('empdata: store:: Checked and resource not found for ip_address('.$ip_address.')');
            $failure['response']['message'] = 'Resource not found';
            return $failure;
        }

        Log::info('empdata: store:: Checked and update a resource which found for ip_address('.$ip_address.').');
        $crud_status = \App\Employee_Web_History::create($create_array);
        //$status = $crud_status ? 'Store the resource in Database success' : 'Store the resource in Database failed';
        return $crud_status ? $success : $failure;
    }
}
