<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee_Web_History extends Model
{
    //use SoftDeletes;

    /**
     * The mapping between model with table.
     *
     * @var string
     */

    protected $table = 'employee_web_history';     
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     * 'payment_transaction_id', 'payment_status' ignored
     * @var array
     */
    protected $fillable = [
    	'ip_address', 'url', 'date'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'    
    ];

    /**
     * Get the IpAddress who owns the history.
     */
    public function IpAddress()
    {
        return $this->belongsTo('\App\Employees','ip_address','ip_address');
    }
}
